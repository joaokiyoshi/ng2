const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const webpackCommonConfig = require('./webpack.common');

module.exports = function production(environment) {
  return webpackMerge(webpackCommonConfig, {
    devtool: 'source-map',
    htmlLoader: {
      minimize: false
    },
    plugins: [
      new webpack.NoErrorsPlugin(),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin({
        mangle: { keep_fnames: true }
      }),
      new webpack.DefinePlugin(environment)
    ]
  });
};
