const webpack = require('webpack');
const path = require('path');
const HTMLPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: {
    polyfills: path.resolve(__dirname, '../src/polyfills.ts'),
    vendor: path.resolve(__dirname, '../src/vendors.ts'),
    app: [path.resolve(__dirname, '../src/index.ts'), path.resolve(__dirname, '../src/index.scss')]
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name].js',
    chunkFilename: '[id].chunk.js'
  },
  resolve: {
    extensions: ['', '.ts', '.js']
  },
  module: {
    loaders: [
      {
        test: /\.ts$/,
        loaders: ['awesome-typescript', 'angular2-template']
      },
      {
        test: /\.html$/,
        loader: 'html'
      },
      {
        test: /\.scss$/,
        include: path.resolve(__dirname, '../src/app'),
        loader: 'raw!sass'
      },
      {
        test: /\.scss$/,
        exclude: path.resolve(__dirname, '../src/app'),
        loader: ExtractTextPlugin.extract('css?sourcemap!sass?sourcemap')
      }
    ]
  },
  plugins: [
    new CleanPlugin(['dist'], {
      root: path.resolve(__dirname, '..')
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: ['app', 'vendor', 'polyfills']
    }),
    new HTMLPlugin({
      template: path.resolve(__dirname, '../src/index.html'),
      hash: true
    }),
    new ExtractTextPlugin('styles.css'),
    new CopyPlugin([
      {
        context: path.resolve(__dirname, '../src'),
        from: '**/*.+(png|jpeg|jpg|gif|svg|woff|woff2|ttf|eot|ico)',
        to: path.resolve(__dirname, '../dist/[path][name].[ext]')
      },
      {
        context: path.resolve(__dirname, '../src'),
        from: 'unsupported.html',
        to: path.resolve(__dirname, '../dist/[path][name].[ext]')
      }
    ])
  ]
};
