const webpack = require('webpack');
const path = require('path');

module.exports = function (environment) {
  return {
    devtool: 'inline-source-map',
    resolve: {
      extensions: ['', '.ts', '.js']
    },
    module: {
      loaders: [
        {
          test: /\.ts$/,
          loaders: ['awesome-typescript', 'angular2-template']
        },
        {
          test: /\.html$/,
          loader: 'html'
        },
        {
          test: /.scss$/,
          exclude: path.resolve(__dirname, '../src/app'),
          loader: 'null'
        },
        {
          test: /.scss$/,
          include: path.resolve(__dirname, '../src/app'),
          loader: 'raw!scss'
        }
      ]
    },
    plugins: [
      new webpack.DefinePlugin(environment)
    ]
  };
};
