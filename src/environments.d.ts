declare const ENABLE_PROD_MODE : boolean;

interface GlobalEnvironment {
  ENABLE_PROD_MODE:boolean;
}
