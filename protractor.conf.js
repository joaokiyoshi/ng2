const HTMLScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');
const jasmineReporters = require('jasmine-reporters');
const path = require('path');

require('ts-node/register');

exports.config = {
  baseUrl: 'http://localhost:8080',
  capabilities: {
    browserName: 'chrome'
  },
  specs: [
    './e2e/**/*.spec.ts'
  ],
  framework: 'jasmine',
  jasmineNodeOpts: {
    showTiming: true,
    showColors: true,
    defaultTimeoutInterval: 120000,
    isVerbose: false,
    includeStackTrace: false
  },
  directConnect: true,
  allScriptsTimeout: 120000,
  getPageTimeout: 120000,
  useAllAngular2AppRoots: true,
  onPrepare: () => {
    const moment = require('moment');
    const faker = require('faker');

    faker.locale = 'pt_BR';

    browser.ignoreSynchronization = true;
    browser.driver.manage().window().setSize(1600, 1000);

    jasmine.getEnv().addReporter(new HTMLScreenshotReporter({
      dest: `./e2e/reports/${moment().format('DD_MM_YYYY_HH_mm_ss')}`,
      filename: 'report.html'
    }));

    jasmine.getEnv().addReporter(new jasmineReporters.TerminalReporter({
      color: true,
      verbosity: 3,
      showStack: true
    }));
  }
};
