import { Component } from '@angular/core';

@Component({
  selector: 'app-sample',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  message: string;

  constructor() {
    this.message = 'Angular 2 Sample App';
  }
}
