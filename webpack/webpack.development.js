const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const webpackCommonConfig = require('./webpack.common');

module.exports = function development(environment) {
  return webpackMerge(webpackCommonConfig, {
    devtool: 'source-map',
    devServer: {
      historyApiFallback: true,
      stats: 'minimal',
      contentBase: 'dist',
      outputPath: 'dist'
    },
    module: {
      preLoaders: [
        {
          test: /\.ts$/,
          loader: 'tslint-loader'
        }
      ]
    },
    plugins: [
      new webpack.DefinePlugin(environment)
    ]
  });
};
