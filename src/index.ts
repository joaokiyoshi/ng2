/* Angular 2 bootstrapper */
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';

/* Module to bootstrap */
import { AppModule } from './app/app.module';

if (ENABLE_PROD_MODE) {
  enableProdMode();
}

/* Bootstrap */
platformBrowserDynamic().bootstrapModule(AppModule);
