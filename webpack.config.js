const fs = require('fs');

function existsFile(filePath) {
  try {
    fs.statSync(filePath);
    return true;
  } catch (_) {
    return false;
  }
}

const environment = process.argv.includes('--env') ? process.argv[process.argv.indexOf('--env') + 1] : 'development';

if (!existsFile(`${__dirname}/environments/${environment}.json`)) {
  throw new Error(`File environments/${environment}.json does not exists!`);
}

if (!existsFile(`${__dirname}/webpack/webpack.${environment}.js`)) {
  throw new Error(`File webpack/webpack.${environment}.js does not exists!`);
}

module.exports = require(`./webpack/webpack.${environment}`)(require(`./environments/${environment}.json`));
