const webpackConfig = require('./webpack/webpack.test');
const environment = require('./environments/test.json');

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    browsers: ['PhantomJS'],
    files: [
      { pattern: './karma-test-bootstrap.js', watched: false }
    ],
    preprocessors: {
      './karma-test-bootstrap.js': ['webpack', 'sourcemap']
    },
    webpack: webpackConfig(environment),
    webpackMiddleware: {
      stats: 'errors-only'
    },
    webpackServer: {
      noInfo: true
    },
    reporters: ['mocha'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    singleRun: true
  });
};
