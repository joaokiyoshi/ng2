import { ElementFinder, browser, by, element } from 'protractor';

describe('App', () => {
  beforeEach(() => {
    browser.get('/');
  });

  it('should have a message', () => {
    const subject: ElementFinder = element(by.css('h1'));
    expect(subject.getText()).toEqual('Angular 2 Sample App');
  });
});
