import { TestBed } from '@angular/core/testing';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({ declarations: [AppComponent] });
  });

  it('should work', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const appComponent = fixture.componentInstance;

    expect(appComponent instanceof AppComponent).toBe(true);
  });

  it('should have a message', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const appComponent = fixture.componentInstance;

    expect(appComponent.message).toEqual('Angular 2 Sample App');
  });
});
